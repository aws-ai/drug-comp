import time

import boto3
import requests
from requests_aws4auth import AWS4Auth

from opensearch.util.opensearch_queries import get_drug_data_from_index


def lex_intent_get_slots(intent_request):
    return intent_request['sessionState']['intent']['slots']


def lex_intent_get_slot(intent_request, slot_name):
    slots = lex_intent_get_slots(intent_request)
    if slots is not None and slot_name in slots and slots[slot_name] is not None:
        return slots[slot_name]['value']['interpretedValue']
    else:
        return None


def get_session_attributes(intent_request):
    session_state = intent_request['sessionState']
    if 'sessionAttributes' in session_state:
        return session_state['sessionAttributes']

    return {}


def elicit_intent(intent_request, session_attributes, message):
    return {
        'sessionState': {
            'dialogAction': {
                'type': 'ElicitIntent'
            },
            'sessionAttributes': session_attributes
        },
        'messages': [message] if message is not None else None,
        'requestAttributes': intent_request['requestAttributes'] if 'requestAttributes' in intent_request else None
    }


def close(intent_request, session_attributes, fulfillment_state, message):
    intent_request['sessionState']['intent']['state'] = fulfillment_state
    return {
        'sessionState': {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close'
            },
            'intent': intent_request['sessionState']['intent']
        },
        'messages': [message],
        'sessionId': intent_request['sessionId'],
        'requestAttributes': intent_request['requestAttributes'] if 'requestAttributes' in intent_request else None
    }


def detect_entities_using_comprehend_medical(text: str):
    comprehend_medical_client = boto3.client('comprehendmedical')
    result = comprehend_medical_client.detect_entities_v2(Text=text)
    entities = result['Entities']
    filtered_drugs = [e['Text'] for e in entities if e['Type'] == 'GENERIC_NAME' and e['Score'] >= 0.6]
    filtered_medical_conditions = [e['Text'] for e in entities if
                                   e['Category'] == 'MEDICAL_CONDITION' and e['Score'] >= 0.6]
    return filtered_drugs, filtered_medical_conditions


def get_auth_header_opensearch(region):
    credentials = boto3.Session().get_credentials()
    awsauth = AWS4Auth(credentials.access_key, credentials.secret_key,
                       region, 'es', session_token=credentials.token)
    return awsauth


def report_drug_side_effect(drug, side_effect, user_id):

    host = 'https://search-drug-comp-cih2zytdo5n2oucnqd6bwypc5y.us-east-1.es.amazonaws.com/drug-comp'
    url = host + '/drug-interactions'
    headers = {"Content-Type": "application/json"}
    awsauth = get_auth_header_opensearch('us-east-1')

    document = {
        'drug': drug,
        'side_effect': side_effect,
        "user_id": user_id,
        "interaction_type": 'user-reported-side-effect',
        "timestamp": time.time()
    }

    r = requests.post(url, json=document, headers=headers, auth=awsauth)


def query_opensearch(drugs, conditions, intent_name, session_id):
    drugs = drugs[0]
    if intent_name == 'AskForDrugInteractions':
        drug_interactions = get_drug_data_from_index('drug-comp', drugs, 'drug-interaction', 'related_drug')
        messages = []
        if drug_interactions and len(drug_interactions):
            messages.append(f'I have identified the following drugs which have an interaction with {drugs}: ' \
                            + ','.join(drug_interactions))
        predicted_interactions = get_drug_data_from_index('drug-comp', drugs, 'predicted-interaction', 'related_drug')
        if predicted_interactions and len(predicted_interactions):
            messages.append(f"Also, be careful if you're taking any of the drugs: {','.join(predicted_interactions)}."
                            f"There might exist a potential interaction between them and {drugs}, but I'm not sure.")
        if not len(messages):
            return f"I haven't found any drugs which interact with {drugs}"
        return '\n'.join(messages)
    elif intent_name == 'AskForDrugSideEffects':
        drug_side_effects = get_drug_data_from_index('drug-comp', drugs, 'adverse-reaction', 'medical_condition')
        if drug_side_effects and len(drug_side_effects):
            return f'I have identified the following side effects associated with {drugs}: ' \
                   + ','.join(drug_side_effects)
        else:
            return f"I haven't found any side effects associated with {drugs}"
    elif intent_name == 'ReportDrugSideEffect':
        if conditions and len(conditions):
            for condition in conditions:
                report_drug_side_effect(drugs, condition, session_id)
            return f'I have reported the following side effects for the drug {drugs}: ' + ','.join(conditions)
        else:
            return f"I haven't identified any side effects to report for the drug {drugs}"
    elif intent_name == 'AskForDrugUsage':
        drug_indications = get_drug_data_from_index('drug-comp', drugs, 'indication', 'medical_condition')
        if drug_indications and len(drug_indications):
            return f'I have identified the following usage and indications associated with {drugs}: ' \
                   + ','.join(drug_indications)
        else:
            return f"I haven't found any reported indications for the drug {drugs}"

    raise Exception('Intent with name ' + intent_name + ' not supported')


def dispatch(intent_request):
    intent_name = intent_request['sessionState']['intent']['name']
    session_attributes = get_session_attributes(intent_request)
    session_id = intent_request['sessionId']

    text = intent_request['inputTranscript']
    drugs, conditions = detect_entities_using_comprehend_medical(text)

    if not drugs or not len(drugs):
        query_result = "I haven't identified any drugs in your question." \
                       " Can you please try to ask it in a different way?"
    else:
        query_result = query_opensearch(drugs, conditions, intent_name, session_id)

    message = {
        'contentType': 'PlainText',
        'content': query_result
    }
    fulfillment_state = "Fulfilled"
    return close(intent_request, session_attributes, fulfillment_state, message)


""" --- Main handler --- """


def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """

    return dispatch(event)
