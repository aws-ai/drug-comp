import boto3
import requests
import time
from tqdm import tqdm
from requests_aws4auth import AWS4Auth

credentials = boto3.Session().get_credentials()
client = boto3.client('opensearch')
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key,
                   'us-east-1', 'es', session_token=credentials.token)

host = 'https://search-drug-comp-cih2zytdo5n2oucnqd6bwypc5y.us-east-1.es.amazonaws.com/drug-comp'

headers = {"Content-Type": "application/json"}

s3 = boto3.client('s3')


def upload_data(bucket, key, type):
    url = host + '/drug-interactions'
    obj = s3.get_object(Bucket=bucket, Key=key)
    body = obj['Body'].read()
    lines = body.splitlines()

    # Match the regular expressions to each line and index the JSON
    idx, field_name1, field_name2 = lines[0].decode('utf-8').split(',')
    for line in tqdm(lines[1:]):
        line = line.decode("utf-8")
        idx, field_1, field_2 = line.split(',')
        document = {field_name1: field_1, field_name2: field_2, "interaction_type": type, "timestamp": time.time()}
        r = requests.post(url, json=document, headers=headers, auth=awsauth)
        print(r.content)


if __name__ == '__main__':
    bucket = 'drug-comp'

    key_and_entity_type = [
        ('transformations/drug_interactions_merged.csv', 'drug-interaction'),
        ('transformations/indications_merged.csv', 'indication'),
        ('transformations/adverse_reactions_merged.csv', 'adverse-reaction')
    ]

    [upload_data(bucket, key, type) for key, type in key_and_entity_type]

