import boto3
import requests
import json

from requests_aws4auth import AWS4Auth


def get_drug_data_from_index(
        index: str,
        drug: str,
        type: str,
        field_to_return: str
):
    credentials = boto3.Session().get_credentials()
    awsauth = AWS4Auth(credentials.access_key, credentials.secret_key,
                       'us-east-1', 'es', session_token=credentials.token)

    host = 'https://search-drug-comp-cih2zytdo5n2oucnqd6bwypc5y.us-east-1.es.amazonaws.com'
    url = f'{host}/{index}/_search?q=drug:{drug}'
    response = requests.get(url, auth=awsauth)
    hits = json.loads(response.content.decode()).get('hits')
    if not hits:
        return

    related_results = []
    for doc in hits['hits']:
        if 'interaction_type' in doc['_source'] and doc['_source']['interaction_type'] == type:
            related_results.append(doc['_source'][field_to_return])

    if len(related_results):
        return related_results


if __name__ == '__main__':
    res = get_drug_data_from_index('drug-comp', 'aspirin', 'user-reported-side-effect', 'side_effect')
    print(res)
