import json

import boto3
import botocore
import time


class OpenSearchService:
    def __init__(self, domain_name):
        self._client = boto3.client('opensearch')
        self.domain_name = domain_name
        self._access_policy = self._create_access_policy_to_allow_actions_for_every_user()

    def _create_access_policy_to_allow_actions_for_every_user(self):
        policy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": "*"
                    },
                    "Action": "es:*",
                    "Resource": f"arn:aws:es:us-east-1:714228358652:domain/{self.domain_name}/*"
                }
            ]
        }
        return json.dumps(policy)

    def create_domain(
            self,
            engine_version: str,
            instance_type: str,
            instance_count: int,
            dedicated_master_enabled: bool,
            dedicated_master_instance_type: str = None,
            dedicated_master_instance_count: int = None,
            ebs_enabled: bool = False,
            ebs_volume_type: str = None,
            ebs_volume_size: int = None,
            node_to_node_encryption_enabled: bool = False,
            master_user_arn: str = None
    ):
        """Creates an Amazon OpenSearch Service domain with the specified options."""
        cluster_config = {
            'InstanceType': instance_type,
            'InstanceCount': instance_count,
            'DedicatedMasterEnabled': dedicated_master_enabled,
        }
        if dedicated_master_enabled:
            cluster_config['DedicatedMasterType'] = dedicated_master_instance_type
            cluster_config['DedicatedMasterCount'] = dedicated_master_instance_count

        ebs_options = {
            'EBSEnabled': ebs_enabled
        }

        if ebs_enabled:
            ebs_options['VolumeType'] = ebs_volume_type
            ebs_options['VolumeSize'] = ebs_volume_size

        response = self._client.create_domain(
            DomainName=self.domain_name,
            EngineVersion=engine_version,
            ClusterConfig=cluster_config,
            EBSOptions=ebs_options,
            AccessPolicies=self._access_policy,
            NodeToNodeEncryptionOptions={
                'Enabled': node_to_node_encryption_enabled
            },
            AdvancedSecurityOptions={
                'Enabled': True,
                'MasterUserOptions': {
                    'MasterUserARN': master_user_arn
                },
            },
            EncryptionAtRestOptions={
                'Enabled': True
            },
            DomainEndpointOptions={
                'EnforceHTTPS': True
            }
        )
        return response

    def update_domain(
            self,
            instance_type: str,
            instance_count: int,
            dedicated_master_enabled: bool,
            dedicated_master_instance_type: str,
            dedicated_master_instance_count: str
    ):
        try:
            response = self._client.update_domain_config(
                DomainName=self.domain_name,
                ClusterConfig={
                    'InstanceType': instance_type,
                    'InstanceCount': instance_count,
                    'DedicatedMasterEnabled': dedicated_master_enabled,
                    'DedicatedMasterType': dedicated_master_instance_type,
                    'DedicatedMasterCount': dedicated_master_instance_count
                },
            )

        except botocore.exceptions.ClientError as error:
            if error.response['Error']['Code'] == 'ResourceNotFoundException':
                print('Domain not found. Please check the domain name.')
            else:
                raise error

    def delete_domain(self):
        """Deletes an OpenSearch Service domain. Deleting a domain can take several minutes."""
        try:
            response = self._client.delete_domain(
                DomainName=self.domain_name
            )
            return response
        except botocore.exceptions.ClientError as error:
            if error.response['Error']['Code'] == 'ResourceNotFoundException':
                print('Domain not found. Please check the domain name.')
            else:
                raise error

    def wait_for_domain_processing(self):
        """Waits for the domain to finish processing changes."""
        try:
            response = self._client.describe_domain(
                DomainName=self.domain_name
            )
            # Every 15 seconds, check whether the domain is processing.
            while response["DomainStatus"]["Processing"]:
                print('Domain still processing...')
                time.sleep(15)
                response = self._client.describe_domain(DomainName=self.domain_name)

            # Once we exit the loop, the domain is available.
            return response

        except botocore.exceptions.ClientError as error:
            if error.response['Error']['Code'] == 'ResourceNotFoundException':
                print('Domain not found. Please check the domain name.')
            else:
                raise error
