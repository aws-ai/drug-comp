import boto3
import logging

from lex.util.locale_ids import LexLanguageLocale


class LexBot:
    def __init__(
            self,
            bot_name: str,
            description: str,
            role_arn: str,
            is_child_directed: bool,
            idle_session_ttl_seconds: int = 60,
            bot_tags: dict = None,
            test_bot_alias_tags: dict = None,
    ):
        self._client = boto3.client('lexv2-models')
        self.bot_name = bot_name
        self.description = description
        self.role_arn = role_arn
        self.is_child_directed = is_child_directed
        self.idle_session_ttl_seconds = idle_session_ttl_seconds
        self.bot_tags = bot_tags if bot_tags else {'segment': 'user interaction'}
        self.test_bot_alias_tags = test_bot_alias_tags if test_bot_alias_tags else {'releasetype': 'dev'}
        self.bot_id = self._try_create_bot()
        self.version = 'DRAFT'

    def _try_create_bot(self):
        try:
            waiter = self._client.get_waiter('bot_available')

            response = self._client.create_bot(
                botName=self.bot_name,
                description=self.description,
                roleArn=self.role_arn,
                dataPrivacy={
                    'childDirected': self.is_child_directed
                },
                idleSessionTTLInSeconds=self.idle_session_ttl_seconds,
                botTags=self.bot_tags,
                testBotAliasTags=self.test_bot_alias_tags
            )

            waiter.wait(
                botId=response['botId'],
                WaiterConfig={
                    'Delay': 123,
                    'MaxAttempts': 123
                }
            )

            return response['botId']
        except Exception as e:
            logging.error(e, exc_info=True)
            raise

    def create_bot_locale(
            self,
            language_locale: LexLanguageLocale,
            description: str,
            min_intent_confidence_threshold: float,
            voice_id: str,
            engine: str = 'standard',
    ):
        try:
            response = self._client.create_bot_locale(
                botId=self.bot_id,
                botVersion=self.version,
                localeId=language_locale.value,
                description=description,
                nluIntentConfidenceThreshold=min_intent_confidence_threshold,
                voiceSettings={
                    'voiceId': voice_id,
                    'engine': engine
                }
            )
            return response
        except Exception as e:
            logging.error(e, exc_info=True)
            raise

    def create_intent(
            self,
            intent_name: str,
            description: str,
            language_locale: LexLanguageLocale,
            utterances: list[str],
            dialog_code_hook_enabled: bool,
            fulfillment_code_hook_enabled: bool,
            fulfillment_successful_message: str = None,
            fulfillment_failed_message: str = None,
    ):
        try:
            success_response = {
                'messageGroups': [
                    {
                        'message': {
                            'plainTextMessage': {
                                'value': fulfillment_successful_message
                            },
                        },
                    }
                ]
            }

            fail_response = {
                'messageGroups': [
                    {
                        'message': {
                            'plainTextMessage': {
                                'value': fulfillment_failed_message
                            }
                        }
                    }
                ]
            }

            fulfillment_code_hook = {
                'enabled': fulfillment_code_hook_enabled
            }

            if any([fulfillment_failed_message, fulfillment_successful_message]):
                fulfillment_code_hook["postFulfillmentStatusSpecification"] = {}
                if fulfillment_successful_message:
                    fulfillment_code_hook["postFulfillmentStatusSpecification"]['successResponse'] = success_response
                if fulfillment_failed_message:
                    fulfillment_code_hook["postFulfillmentStatusSpecification"]["failureResponse"] = fail_response

            response = self._client.create_intent(
                intentName=intent_name,
                description=description,
                sampleUtterances=[{'utterance': u} for i, u in enumerate(utterances)],
                dialogCodeHook={
                    'enabled': dialog_code_hook_enabled
                },
                fulfillmentCodeHook=fulfillment_code_hook,
                botId=self.bot_id,
                botVersion=self.version,
                localeId=language_locale.value,
            )
            return response
        except Exception as e:
            logging.error(e, exc_info=True)

    def create_bot_alias(
            self,
            alias_name: str,
            description: str,
            lambda_arn: str = None,
    ):
        try:
            response = self._client.create_bot_alias(
                botAliasName=alias_name,
                description=description,
                botVersion=self.version,
                botAliasLocaleSettings={
                    'string': {
                        'enabled': True if lambda_arn else False,
                        'codeHookSpecification': {
                            'lambdaCodeHook': {
                                'lambdaARN': lambda_arn,
                                'codeHookInterfaceVersion': 'string'
                            }
                        }
                    }
                },
                conversationLogSettings={
                    'textLogSettings': [
                        {
                            'enabled': False,
                        },
                    ],
                    'audioLogSettings': [
                        {
                            'enabled': False,
                        },
                    ]
                },
                sentimentAnalysisSettings={
                    'detectSentiment': False
                },
                botId=self.bot_id,
                tags=self.test_bot_alias_tags
            )
            return response
        except Exception as e:
            logging.error(e, exc_info=True)
            raise

    def update_test_bot_alias(
            self,
            description,
            language_locale: LexLanguageLocale,
            lambda_arn=None,
    ):
        aliases = self._client.list_bot_aliases(botId=self.bot_id)
        test_alias = [alias for alias in aliases['botAliasSummaries'] if alias['botAliasName'] == 'TestBotAlias']

        response = self._client.update_bot_alias(
            botId=self.bot_id,
            botAliasId=test_alias[0]['botAliasId'],
            botAliasName=test_alias[0]['botAliasName'],
            description=description,
            botVersion=test_alias[0]['botVersion'],
            botAliasLocaleSettings={
                language_locale.value: {
                    'enabled': True if lambda_arn else False,
                    'codeHookSpecification': {
                        'lambdaCodeHook': {
                            'lambdaARN': lambda_arn,
                            'codeHookInterfaceVersion': '1.0',
                        }
                    }
                }
            }
        )
