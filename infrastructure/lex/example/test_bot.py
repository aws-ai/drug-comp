import boto3

from lex.util.locale_ids import LexLanguageLocale

if __name__ == '__main__':
    client = boto3.client('lexv2-runtime')
    bot_id = 'EDOC7YWWXA'
    bot_alias_id = 'TSTALIASID'

    response = client.recognize_text(
        botId=bot_id,
        botAliasId=bot_alias_id,
        localeId=LexLanguageLocale.en_US.value,
        sessionId='123',  # should be generated for each session that the user starts on the app
        text='Can you tell me what other drugs does voriconazole interact with?'
    )

    print(response)
