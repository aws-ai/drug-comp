from enum import Enum


class LexLanguageLocale(Enum):
    ca_ES = 'ca_ES'
    de_AT = 'de_AT'
    de_DE = 'de_DE'
    en_AU = 'en_AU'
    en_GB = 'en_GB'
    en_IN = 'en_IN'
    en_US = 'en_US'
    en_ZA = 'en_ZA'
    es_419 = 'es_419'
    es_ES = 'es_ES'
    es_US = 'es_US'
    fr_CA = 'fr_CA'
    fr_FR = 'fr_FR'
    it_IT = 'it_IT'
    ja_JP = 'ja_JP'
    ko_KR = 'ko_KR'
    pt_BR = 'pt_BR'
    pt_PT = 'pt_PT'
    zh_CN = 'zh_CN'