import boto3
from zipfile import ZipFile
import os
import shutil
import time


lambda_client = boto3.client('lambda')
role = 'arn:aws:iam::714228358652:role/service-role/LexAskForDrugInteractionsHook-role-d4l5uqrj'


def read_binary_zip_content(files_to_zip: list[str], requirements_file: str, archive_name: str):
    dependencies_folder = f'target-{time.time()}'
    os.system(f"pip3 install -r {requirements_file} --target {dependencies_folder}")

    with ZipFile(archive_name, "w") as zip_file:
        for base, dirs, files in os.walk(dependencies_folder):
            for file in files:
                fn = os.path.join(base, file)
                zip_file.write(fn, fn.replace(f'{dependencies_folder}/', ''))
        [zip_file.write(file) for file in files_to_zip]

    with open(archive_name, 'rb') as f:
        content = f.read()

    os.remove(archive_name)
    shutil.rmtree(dependencies_folder)

    return content


if __name__ == '__main__':
    """
        TODO: Create docker container for lambda instead of zip file
    """

    zipped_function = read_binary_zip_content(
        files_to_zip=['lambda/handlers.py', 'opensearch/util/opensearch_queries.py'],
        requirements_file='lambda/requirements.txt',
        archive_name="lambda.zip")

    response = lambda_client.create_function(
        FunctionName='LexAskForDrugCompTest',
        Runtime='python3.9',
        Role=role,
        Handler='lambda.handlers.lambda_handler',
        Code=dict(ZipFile=zipped_function),
        Timeout=300,  # Maximum allowable timeout
        # Set up Lambda function environment variables
        Environment={
            'Variables': {
                'Name': 'LexAskForDrugInteractions',
                'Environment': 'dev'
            }
        },
    )

    print(response)
