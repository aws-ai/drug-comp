import boto3

from lex.util.locale_ids import LexLanguageLocale
from lex.lex_bot import LexBot

lex_role_arn = "arn:aws:iam::714228358652:role/aws-service-role/lexv2.amazonaws.com/AWSServiceRoleForLexV2Bots_milena"

fulfillment_failed_message = 'Something went wrong. Please try to restate your question.'


def get_lambda_arn_from_name(function_name: str):
    client = boto3.client('lambda')
    return client.get_function(FunctionName=function_name)['Configuration']['FunctionArn']


if __name__ == '__main__':
    drug_comp_bot = LexBot(
        bot_name="IntertecDrugComp",
        description="Lex Bot fro DrugComp project",
        role_arn=lex_role_arn,
        is_child_directed=False,
        idle_session_ttl_seconds=300,
    )

    drug_comp_bot.create_bot_locale(
        language_locale=LexLanguageLocale.en_US,
        description="English bot",
        min_intent_confidence_threshold=0.6,
        voice_id="Kendra",
    )

    drug_comp_bot.update_test_bot_alias(
        description='Dev alias for bot in en-US',
        language_locale=LexLanguageLocale.en_US,
        lambda_arn=get_lambda_arn_from_name('LexAskForDrugInteractionsTest')
    )

    drug_comp_bot.create_intent(
        intent_name="AskForDrugInteractions",
        description="The patient asks for known and potential drug-drug interactions",
        language_locale=LexLanguageLocale.en_US,
        utterances=[
            "What other drugs does Azelex interact with?",
            "Can you list the drug interactions for Metronidazole tablets?",
            "What are the medications that Celecoxib capsules, for oral use cannot be taken with at the same time?",
            "Are there any drugs I can't take while using Minocycline Hydrochloride?",
            "I'm using Naproxen delayed-release tablets, are there any medications that have a bad reaction with it?"
        ],
        dialog_code_hook_enabled=False,
        fulfillment_code_hook_enabled=True,
        fulfillment_failed_message=fulfillment_failed_message
    )

    drug_comp_bot.create_intent(
        intent_name="AskForDrugSideEffects",
        description="The patient asks for known side effects for a certain drug",
        language_locale=LexLanguageLocale.en_US,
        utterances=[
            "Can you tell me the side effects from using Aspirin?",
            "Are there any bad reactions I can have from Tretinoin treatment?",
            "Is it possible to have any adverse reactions from Amoxiclav?",
            "What side effects should I expect from my treatment with Olanzapine?"
        ],
        dialog_code_hook_enabled=False,
        fulfillment_code_hook_enabled=True,
        fulfillment_failed_message=fulfillment_failed_message
    )

    drug_comp_bot.create_intent(
        intent_name="ReportDrugSideEffect",
        description="The patient reports a side effect from using a certain drug.",
        language_locale=LexLanguageLocale.en_US,
        utterances=[
            "I'm taking Aspirin and my heart started beating faster.",
            "I want to report a side effect from using Olanzapine, I have trouble sleeping since I started using it.",
            "I got a skin rash after using Cetaphil.",
        ],
        dialog_code_hook_enabled=False,
        fulfillment_code_hook_enabled=True,
        fulfillment_failed_message=fulfillment_failed_message
    )

    drug_comp_bot.create_intent(
        intent_name="AskForDrugUsage",
        description="The patient asks what a drug is used for.",
        language_locale=LexLanguageLocale.en_US,
        utterances=[
            "Can you tell me what is Analgin used for?",
            "I don't know why I should be taking Aspirin.",
            "What is the usage for Letizen?",
            "When should I use Amoxiclav?"
        ],
        dialog_code_hook_enabled=False,
        fulfillment_code_hook_enabled=True,
        fulfillment_failed_message=fulfillment_failed_message
    )
