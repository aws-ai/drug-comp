from opensearch.opensearch_service import OpenSearchService

if __name__ == '__main__':
    service = OpenSearchService('drug-comp')
    service.create_domain(
        engine_version='OpenSearch_1.3',
        instance_type='t3.small.search',
        instance_count=3,
        dedicated_master_enabled=False,
        ebs_enabled=True,
        ebs_volume_type='gp3',
        ebs_volume_size=10,
        node_to_node_encryption_enabled=True,
        master_user_arn='arn:aws:iam::714228358652:role/service-role/LexAskForDrugInteractionsHook-role-d4l5uqrj'
    )
