
from stellargraph import StellarGraph
from stellargraph.mapper import HinSAGELinkGenerator
from stellargraph.layer import HinSAGE, LinkEmbedding
from stellargraph.data import EdgeSplitter
from stellargraph.layer import  HinSAGE, link_classification
from tensorflow import keras
from stellargraph import StellarGraph
import pandas as pd
import numpy as np
import argparse
import time
import tqdm
import os

def purify_df(df,extra_drugs=None,extra_filter=None):
    duplicate_drug = []
    for d in tqdm.tqdm(set(df['drug'].values)):
        for m in set(df['medical_condition'].values):
            if d == m:
                duplicate_drug.append(d)
                #df = df[df['medical_condition']!=duplicate_drug]
    if extra_drugs is not None:
        for d in tqdm.tqdm(set(extra_drugs)):
            for m in set(df['medical_condition'].values):
                if d == m:
                    duplicate_drug.append(d)
    if extra_filter is not None:
        for d in tqdm.tqdm(set(extra_filter.values)):
            for m in set(df['medical_condition'].values):
                if d == m:
                    duplicate_drug.append(d)
                    #df = df[df['medical_condition']!=duplicate_drug]
    df = df[~df['medical_condition'].isin(duplicate_drug)]
    print('Number of duplicate drugs: ',len(duplicate_drug))
    print(duplicate_drug)
    return df


if __name__ == '__main__':
    args = argparse.ArgumentParser()
    args.add_argument('--drug_interactions', type=str, help='Path to the drug interactions dataset')
    args.add_argument('--adverse_reactions', type=str, help='Path to the adverse reactions dataset')
    args.add_argument('--indications', type=str, help='Path to the indications dataset')
    args.add_argument('--epochs', type=int, help='Number of epochs')
    args.add_argument('--batch_size', type=int, help='Batch size')
    args.add_argument('--num_samples', type=int, help='Number of samples')
    args.add_argument('--layer_size', type=int, help='Layer size')
    args.add_argument('-o', '--output', type=str, help='Output path file')
    arguments = args.parse_args()
    
    EPOCHS = arguments.epochs
    BATCH_SIZE = arguments.batch_size
    NUM_SAMPLES = arguments.num_samples
    LAYER_SIZE = arguments.layer_size
    
    
    
    #df = pd.read_csv(arguments.data_path).drop('Unnamed: 0', axis=1)
    drop_column = 'Unnamed: 0'
    drug_interactions = pd.read_csv(arguments.drug_interactions).drop(drop_column, axis=1)
    adverse_reactions = pd.read_csv(arguments.adverse_reactions).drop(drop_column, axis=1)
    indications_reactions = pd.read_csv(arguments.indications).drop(drop_column, axis=1)
    
    drugs_list = set(drug_interactions['drug'].values.tolist()+
                 drug_interactions['related_drug'].values.tolist()+\
                 adverse_reactions['drug'].values.tolist()+\
                     indications_reactions['drug'].values.tolist())
    cleaned_indications_reactions = purify_df(indications_reactions,drugs_list)
    cleaned_adverse_reactions = purify_df(adverse_reactions,drugs_list)
    
    
    drugs = pd.DataFrame(drugs_list,columns=['drug'])
    drugs['feat1'] = np.ones(len(drugs))
    drugs.set_index('drug', inplace=True)

    medical_conditions_list = set(cleaned_adverse_reactions['medical_condition'].values.tolist() + 
                                  cleaned_indications_reactions['medical_condition'].values.tolist() )
    medical_conditions = pd.DataFrame(medical_conditions_list,columns=['medical_condition'])
    medical_conditions['feat1'] = np.ones(len(medical_conditions))
    medical_conditions.set_index('medical_condition', inplace=True)
    
    
    drug_interactions.columns=['source','target']
    cleaned_adverse_reactions.columns=['source','target']
    cleaned_indications_reactions.columns=['source','target']
    
    cleaned_adverse_reactions.set_index(pd.RangeIndex(start = max(drug_interactions.index)+1,
                                                  stop = max(drug_interactions.index)+len(cleaned_adverse_reactions)+1), inplace=True)
    cleaned_indications_reactions.set_index(pd.RangeIndex(start = max(cleaned_adverse_reactions.index)+1,
                                                          stop = max(cleaned_adverse_reactions.index)+len(cleaned_indications_reactions)+1), inplace=True)
    edges = pd.concat([drug_interactions,cleaned_adverse_reactions,cleaned_indications_reactions],axis=0)
    
    
    graph = StellarGraph(nodes = {'drugs':drugs,'medical_conditions':medical_conditions},edges={'drug_interactions':drug_interactions,
                                                                                         'adverse_reactions':cleaned_adverse_reactions,
                                                                                         'indications_reactions':cleaned_indications_reactions})
    
    
    edge_splitter_test = EdgeSplitter(graph)
    test_graph, test_edge_ids, test_edge_labels = edge_splitter_test.train_test_split(p=0.1, method="global", keep_connected=False)
    
    test_drug_drug_edges = np.asarray([edge for edge in test_edge_ids if edge[0] in drugs_list and edge[1] in drugs_list])
    test_drug_drug_labels = np.asarray([label for edge,label in zip(test_edge_ids,test_edge_labels) if edge[0] in drugs_list and edge[1] in drugs_list])
    
    edge_splitter_train = EdgeSplitter(graph)
    train_graph, train_edge_ids, train_edge_labels = edge_splitter_train.train_test_split(p=0.1, method="global", keep_connected=False)
    
    train_drug_drug_edges = np.asarray([edge for edge in train_edge_ids if edge[0] in drugs_list and edge[1] in drugs_list])
    train_drug_drug_labels = np.asarray([label for edge,label in zip(train_edge_ids,train_edge_labels) if edge[0] in drugs_list and edge[1] in drugs_list])

    num_samples = [NUM_SAMPLES,NUM_SAMPLES-2]
    
    train_gen = HinSAGELinkGenerator(train_graph, BATCH_SIZE, num_samples,head_node_types=['drugs','drugs'])
    train_flow = train_gen.flow(train_drug_drug_edges, train_drug_drug_labels, shuffle=True)
    test_gen = HinSAGELinkGenerator(test_graph, BATCH_SIZE, num_samples,head_node_types=['drugs','drugs'])
    test_flow = test_gen.flow(test_drug_drug_edges, test_drug_drug_labels)

    layer_sizes = [LAYER_SIZE, LAYER_SIZE]


    graphsage = HinSAGE(
        layer_sizes=layer_sizes, generator=train_gen, bias=True, dropout=0.3
    )



    x_inp, x_out = graphsage.in_out_tensors()


    prediction = link_classification(edge_embedding_method="concat")(x_out)


    model = keras.Model(inputs=x_inp, outputs=prediction)

    model.compile(
        optimizer=keras.optimizers.Adam(lr=1e-3),
        loss=keras.losses.binary_crossentropy,
        metrics=["acc"],
    )


    init_train_metrics = model.evaluate(train_flow)
    init_test_metrics = model.evaluate(test_flow)

    print("\nTrain Set Metrics of the initial (untrained) model:")
    for name, val in zip(model.metrics_names, init_train_metrics):
        print("\t{}: {:0.4f}".format(name, val))

    print("\nTest Set Metrics of the initial (untrained) model:")
    for name, val in zip(model.metrics_names, init_test_metrics):
        print("\t{}: {:0.4f}".format(name, val))


    history = model.fit(train_flow, epochs=EPOCHS, verbose=2)


    test_metrics = model.evaluate(test_flow)
    print("\nTest Set Metrics of the final model:")
    print("\tloss={:0.4f}".format(test_metrics[0]))
    print("\taccuracy={:0.4f}".format(test_metrics[1]))

    if not os.path.exists(arguments.output):
        os.mkdir(arguments.output)
    
    timestamp = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())
    
    output = os.path.join(arguments.output, f'{timestamp}_drug_interactions_with_other_hinsage_model.h5')
    model.save(arguments.output)






