
from stellargraph import StellarGraph
from stellargraph.mapper import HinSAGELinkGenerator
from stellargraph.layer import HinSAGE, LinkEmbedding
from stellargraph.data import EdgeSplitter
from stellargraph.layer import  HinSAGE, link_classification
from tensorflow import keras
from stellargraph import StellarGraph
import pandas as pd
import numpy as np
import argparse
import time
import os


if __name__ == '__main__':
    args = argparse.ArgumentParser()
    args.add_argument('--data_path', type=str, describe='Path to the data')
    args.add_argument('--epochs', type=int, describe='Number of epochs')
    args.add_argument('--batch_size', type=int, describe='Batch size')
    args.add_argument('--num_samples', type=int, describe='Number of samples')
    args.add_argument('--layer_size', type=int, describe='Layer size')
    args.add_argument('-o', '--output', type=str, describe='Output path file')
    arguments = args.parse_args()
    
    EPOCHS = arguments.epochs
    BATCH_SIZE = arguments.batch_size
    NUM_SAMPLES = arguments.num_samples
    LAYER_SIZE = arguments.layer_size
    
    
    
    df = pd.read_csv(arguments.data_path).drop('Unnamed: 0', axis=1)
    df.columns = ['source','target']

    drugs = pd.DataFrame(set(df['source'].values.tolist()+df['target'].values.tolist()), columns=['drug'])
    drugs['feat1'] = np.ones(len(drugs))
    drugs.set_index('drug', inplace=True)


    graph = StellarGraph(nodes = {'drugs':drugs},edges={'drug_interactions':df})


    edge_splitter_test = EdgeSplitter(graph)
    test_graph, test_edge_ids, test_edge_labels = edge_splitter_test.train_test_split(p=0.1, method="global", keep_connected=False)


    edge_splitter_train = EdgeSplitter(graph)
    train_graph, train_edge_ids, train_edge_labels = edge_splitter_train.train_test_split(p=0.1, method="global", keep_connected=False)

    num_samples = [NUM_SAMPLES,NUM_SAMPLES-2]


    train_gen = HinSAGELinkGenerator(train_graph, BATCH_SIZE, num_samples,head_node_types=['drugs','drugs'])
    train_flow = train_gen.flow(train_edge_ids, train_edge_labels, shuffle=True)
    test_gen = HinSAGELinkGenerator(test_graph, BATCH_SIZE, num_samples,head_node_types=['drugs','drugs'])
    test_flow = test_gen.flow(test_edge_ids, test_edge_labels)

    layer_sizes = [LAYER_SIZE, LAYER_SIZE]


    graphsage = HinSAGE(
        layer_sizes=layer_sizes, generator=train_gen, bias=True, dropout=0.3
    )



    x_inp, x_out = graphsage.in_out_tensors()


    prediction = link_classification(edge_embedding_method="concat")(x_out)


    model = keras.Model(inputs=x_inp, outputs=prediction)

    model.compile(
        optimizer=keras.optimizers.Adam(lr=1e-3),
        loss=keras.losses.binary_crossentropy,
        metrics=["acc"],
    )


    init_train_metrics = model.evaluate(train_flow)
    init_test_metrics = model.evaluate(test_flow)

    print("\nTrain Set Metrics of the initial (untrained) model:")
    for name, val in zip(model.metrics_names, init_train_metrics):
        print("\t{}: {:0.4f}".format(name, val))

    print("\nTest Set Metrics of the initial (untrained) model:")
    for name, val in zip(model.metrics_names, init_test_metrics):
        print("\t{}: {:0.4f}".format(name, val))


    history = model.fit(train_flow, epochs=EPOCHS, verbose=2)


    test_metrics = model.evaluate(test_flow)
    print("\nTest Set Metrics of the final model:")
    print("\tloss={:0.4f}".format(test_metrics[0]))
    print("\taccuracy={:0.4f}".format(test_metrics[1]))

    if not os.path.exists(arguments.output):
        os.mkdir(arguments.output)
    
    timestamp = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())
    
    output = os.path.join(arguments.output, f'{timestamp}_drug_interactions_hinsage_model.h5')
    model.save(arguments.output)






