from stellargraph import StellarGraph
from sklearn.model_selection import train_test_split
from stellargraph.mapper import HinSAGELinkGenerator
from stellargraph.layer import HinSAGE, LinkEmbedding
from tensorflow.keras.layers import Reshape, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import binary_crossentropy

import stellargraph as sg
from stellargraph.data import EdgeSplitter
from stellargraph.mapper import GraphSAGELinkGenerator
from stellargraph.layer import GraphSAGE, HinSAGE, link_classification

from tensorflow import keras
from stellargraph import StellarGraph
import pandas as pd
import numpy as np
import tqdm
import argparse
import os
import time


def purify_df(df,extra_drugs=None,extra_filter=None):
    duplicate_drug = []
    for d in tqdm.tqdm(set(df['drug'].values)):
        for m in set(df['medical_condition'].values):
            if d == m:
                duplicate_drug.append(d)
                #df = df[df['medical_condition']!=duplicate_drug]
    if extra_drugs is not None:
        for d in tqdm.tqdm(set(extra_drugs)):
            for m in set(df['medical_condition'].values):
                if d == m:
                    duplicate_drug.append(d)
    if extra_filter is not None:
        for d in tqdm.tqdm(set(extra_filter.values)):
            for m in set(df['medical_condition'].values):
                if d == m:
                    duplicate_drug.append(d)
                    #df = df[df['medical_condition']!=duplicate_drug]
    df = df[~df['medical_condition'].isin(duplicate_drug)]
    print('Number of duplicate drugs: ',len(duplicate_drug))
    print(duplicate_drug)
    return df


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--epochs', type=int, default=2)
    parser.add_argument('--batch-size', type=int, default=32)
    parser.add_argument('--num-samples', type=list, default=[20,10])
    parser.add_argument('--layer-size', type=list, default=[16,16])
    parser.add_argument('--learning-rate', type=float, default=1e-2)

    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--training', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    parser.add_argument('--validation', type=str, default=os.environ['SM_CHANNEL_VALIDATION'])

    args, _ = parser.parse_known_args()

    epochs     = args.epochs
    batch_size = args.batch_size
    num_samples = args.num_samples
    layer_size = args.layer_size
    learning_rate = args.learning_rate

    model_dir  = args.model_dir
    training_dir   = args.training
    validation_dir = args.validation
    
    drug_interactions = pd.read_csv(os.path.join(training_dir, 'drug_interactions_merged.csv')).drop('Unnamed: 0', axis=1)
    adverse_reactions = pd.read_csv(os.path.join(training_dir, 'adverse_reactions_merged.csv')).drop('Unnamed: 0', axis=1)
    
    drugs_list = set(drug_interactions['drug'].values.tolist()+
                 drug_interactions['related_drug'].values.tolist()+\
                 adverse_reactions['drug'].values.tolist())
    
    cleaned_adverse_reactions = purify_df(adverse_reactions,drugs_list)
    
    drugs_list = set(drug_interactions['drug'].values.tolist()+
                 drug_interactions['related_drug'].values.tolist()+\
                 cleaned_adverse_reactions['drug'].values.tolist())
    
    drugs = pd.DataFrame(drugs_list,columns=['drug'])
    drugs['feat1'] = np.ones(len(drugs))
    drugs.set_index('drug', inplace=True)

    medical_conditions_list = set(cleaned_adverse_reactions['medical_condition'].values.tolist())
    medical_conditions = pd.DataFrame(medical_conditions_list,columns=['medical_condition'])
    medical_conditions['feat1'] = np.ones(len(medical_conditions))
    medical_conditions.set_index('medical_condition', inplace=True)

    drug_interactions.columns=['source','target']
    cleaned_adverse_reactions.columns=['source','target']
    
    cleaned_adverse_reactions.set_index(pd.RangeIndex(start = max(drug_interactions.index)+1,
                                                  stop = max(drug_interactions.index)+len(cleaned_adverse_reactions)+1), inplace=True)
    
    edges = pd.concat([drug_interactions,cleaned_adverse_reactions],axis=0)

    graph = StellarGraph(nodes = {'drugs':drugs,'medical_conditions':medical_conditions}, edges={'rel':edges})


    edge_splitter_test = EdgeSplitter(graph)
    test_graph, test_edge_ids, test_edge_labels = edge_splitter_test.train_test_split(p=0.1, method="global", keep_connected=False)
    
    test_drug_drug_edges = np.asarray([edge for edge in test_edge_ids if edge[0] in drugs_list and edge[1] in drugs_list])
    test_drug_drug_labels = np.asarray([label for edge,label in zip(test_edge_ids,test_edge_labels) if edge[0] in drugs_list and edge[1] in drugs_list])


    edge_splitter_train = EdgeSplitter(graph)
    train_graph, train_edge_ids, train_edge_labels = edge_splitter_train.train_test_split(p=0.1, method="global", keep_connected=False)
    
    train_drug_drug_edges = np.asarray([edge for edge in train_edge_ids if edge[0] in drugs_list and edge[1] in drugs_list])
    train_drug_drug_labels = np.asarray([label for edge,label in zip(train_edge_ids,train_edge_labels) if edge[0] in drugs_list and edge[1] in drugs_list])

    train_gen = HinSAGELinkGenerator(train_graph, batch_size, num_samples,head_node_types=['drugs','drugs'])
    train_flow = train_gen.flow(train_drug_drug_edges, train_drug_drug_labels, shuffle=True)
    test_gen = HinSAGELinkGenerator(test_graph, batch_size, num_samples,head_node_types=['drugs','drugs'])
    test_flow = test_gen.flow(test_drug_drug_edges, test_drug_drug_labels)

    graphsage = HinSAGE(layer_sizes=layer_size, generator=train_gen, bias=True, dropout=0.2)


    x_inp, x_out = graphsage.in_out_tensors()


    prediction = link_classification(edge_embedding_method="concat")(x_out)


    model = keras.Model(inputs=x_inp, outputs=prediction)

    model.compile(
        optimizer=keras.optimizers.Adam(lr=learning_rate),
        loss=keras.losses.binary_crossentropy,
        metrics=["acc"],
    )


    init_train_metrics = model.evaluate(train_flow)
    init_test_metrics = model.evaluate(test_flow)

    print("\nTrain Set Metrics of the initial (untrained) model:")
    for name, val in zip(model.metrics_names, init_train_metrics):
        print("\t{}: {:0.4f}".format(name, val))

    print("\nTest Set Metrics of the initial (untrained) model:")
    for name, val in zip(model.metrics_names, init_test_metrics):
        print("\t{}: {:0.4f}".format(name, val))


    history = model.fit(train_flow, epochs=epochs, verbose=2)


    test_metrics = model.evaluate(test_flow)
    print("\nTest Set Metrics of the final model:")
    print("\tloss={:0.4f}".format(test_metrics[0]))
    print("\taccuracy={:0.4f}".format(test_metrics[1]))

    
    timestamp = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())
    
    
    model.save(os.path.join(model_dir, f'{timestamp}_hinsage_model.h5'))