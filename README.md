![DrugComp](assets/logo.png)

Project for extracting drugs’ incompatibility with other medications, as well as providing timely information to the users that require medical help.

## Prerequisites
1. [AWS account with SSH access for git setup](https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html?icmpid=docs_acc_console_connect_np)
2. [Anaconda for automatic setup locally](https://docs.anaconda.com/anaconda/install)
3. [TMUX](https://github.com/tmux/tmux/wiki)

## Project setup
Clone the project from the repository
```
git clone ssh://git-codecommit.eu-central-1.amazonaws.com/v1/repos/DrugComp
```
Change root project directory
```
cd DrugComp
```
Create the virtual environment
```
conda env create -f environment.yml
```
Activate the virtual environment
```
conda activate drug-comp
```

## Update project
Update the environment with new packages
```
conda env update -f environment.yml
```

## Notebooks
All notebooks are stored in the **notebooks** directory. Since **PyCharm** community edition does not support operations with **ipynb** files, each notebook can be opened with the **Jupyter** IDE running this command:
```
jupyter notebook notebooks/{NOTEBOOK_FILENAME}.ipynb
```

## Usage
The most straightforward way to run the application after it has been deployed on a server, is to start a new TMUX session:
```
tmux new -s StreamSession
```
Starting the conversational interface is done by executing: 
```
streamlit run conversation.py
```
For debugging purposes, the script path should point to the **Streamlit** bin and the following line should be added while acting as parameters:
```
run conversation.py
```
![diagram](assets/diagram.png)