import boto3
import streamlit as st

from PIL import Image
from streamlit_chat import message


def get_text():
    input_key = 'input_' + str(st.session_state['input_key'])
    input_text = text_input_widget.text_input("You: ", "", key=input_key)
    return input_text


icon = Image.open('assets/icon.png')
st.set_page_config(
    page_title="DrugComp",
    page_icon=icon
)

image = Image.open('assets/logo.png')
st.image(image, width=700)

# st.markdown(
#     f"""
#      <style>
#      .stApp {{
#          background-color: white;
#          color: red;
#      }}
#      </style>
#      """,
#     unsafe_allow_html=True
# )

client = boto3.client('lexv2-runtime')
bot_id = 'EDOC7YWWXA'
bot_alias_id = 'TSTALIASID'


text_input_widget = st.empty()


if 'generated' not in st.session_state:
    st.session_state['generated'] = []

if 'past' not in st.session_state:
    st.session_state['past'] = []

if 'input_key' not in st.session_state:
    st.session_state['input_key'] = 0

user_input = get_text()

if user_input:
    st.session_state['input_key'] += 1
    input_key = 'input_' + str(st.session_state['input_key'])
    text_input_widget.text_input("You: ", "", key=input_key)

    with st.spinner('waiting for response...'):
        output = client.recognize_text(
            botId=bot_id,
            botAliasId=bot_alias_id,
            localeId='en_US',
            sessionId='123',  # should be generated for each session that the user starts on the app
            text=user_input
        )

    st.session_state.past.append(user_input)

    response = 'No comment'
    if 'messages' in output:
        response = output['messages'][0]['content']

    st.session_state.generated.append(response)

if st.session_state['generated']:
    for i in range(len(st.session_state['generated']) - 1, -1, -1):
        message(st.session_state["generated"][i], key=str(i), avatar_style="bottts", seed=41)
        message(st.session_state['past'][i], is_user=True, key=str(i) + '_user', avatar_style="avataaars", seed=24)
