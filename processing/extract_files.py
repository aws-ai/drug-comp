import glob
import zipfile


INPUT_PATH = '../data/raw_data/prescription'
OUTPUT_PATH = f'{INPUT_PATH}/xml'


def extract_files_with_extension_from_zip(file_path, output_dir, extension=None):
    with zipfile.ZipFile(file_path, 'r') as zip_ref:
        if not extension:
            zip_ref.extractall(file_path, output_dir)
        else:
            files_to_extract = [x for x in zip_ref.namelist() if x.endswith(extension)]
            [zip_ref.extract(file, OUTPUT_PATH) for file in files_to_extract]


if __name__ == '__main__':
    [extract_files_with_extension_from_zip(file, OUTPUT_PATH, '.xml') for file in glob.glob(f"{INPUT_PATH}/zip/*")]
