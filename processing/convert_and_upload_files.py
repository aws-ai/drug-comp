import xml.etree.ElementTree as ET
import re
import boto3
import logging
import sys

RAW_DATA_PREFIX = 'raw-data'
PROCESSED_DATA_PREFIX = 'processed-data'
SIZE_LIMIT = 40000  # 40KB
SECTIONS_TO_GET = {
    'DRUG INTERACTIONS SECTION': 'drug_interactions',
    'ADVERSE REACTIONS SECTION': 'adverse_reactions',
    'INDICATIONS & USAGE SECTION': 'indications_usage'
}
BUCKET = 'drug-comp'


# Preprocessing and parsing functions
def parse_xml(prescription):
    return ET.fromstring(prescription)


def preprocess_text(text):
    text = re.sub('[^a-zA-Z0-9() \n\.]', '', text).replace('\n', ' ')
    text = re.sub('\(\d+\)', '', text)  # remove numbers from ordered list
    return str.strip(re.sub(' +', ' ', text))


def get_generic_medicine_name(root):
    try:
        generic = root.find(".//{urn:hl7-org:v3}genericMedicine/{urn:hl7-org:v3}name").text
        text = str.lower(preprocess_text(generic))
        return text
    except:
        return


def get_section_by_display_name(root, display_name):
    section = root.find(f".//*[@displayName='{display_name}']...")
    if not section:
        return
    text = ET.tostring(section, encoding='utf-8', method='text').decode()
    return preprocess_text(text)


# S3 functions
def create_s3_bucket_if_not_exists(s3_client, bucket_name):
    try:
        s3_client.create_bucket(Bucket=bucket_name)
        return True
    except Exception as e:
        logging.error(e)
        return False


def upload_file_to_s3(s3_client, bucket, key, content=None, path=None):
    try:
        if path:
            s3_client.upload_file(path, bucket, key)
        elif content:
            s3_client.put_object(
                Bucket=bucket,
                Key=key,
                Body=content
            )
        else:
            return False
        return True
    except Exception as e:
        logging.error(e)
        return False


def get_bucket_resource(bucket):
    s3 = boto3.resource('s3')
    return s3.Bucket(bucket)


if __name__ == '__main__':
    # Create s3 bucket
    s3_client = boto3.client('s3')
    create_s3_bucket_if_not_exists(s3_client, BUCKET)

    # Process files
    large_prescriptions = []
    empty_prescriptions = []
    no_generic_name = 0
    generic_name_to_file_path = {}

    bucket = get_bucket_resource(BUCKET)

    for obj in bucket.objects.filter(Prefix=RAW_DATA_PREFIX):
        uri = f"s3://{BUCKET}/{obj.key}"
        content = obj.get()['Body'].read().decode()

        parsed_prescription = parse_xml(content)
        generic_name = get_generic_medicine_name(parsed_prescription)

        if not generic_name:
            no_generic_name += 1
            continue

        for section in SECTIONS_TO_GET.keys():
            text = get_section_by_display_name(parsed_prescription, section)
            section_name = SECTIONS_TO_GET[section]
            output_file_name = uri.split('/')[-1].replace('.xml', '.txt')

            if not text:
                empty_prescriptions.append(generic_name)
                continue

            if sys.getsizeof(text) > SIZE_LIMIT:
                large_prescriptions.append(generic_name)
                continue

            s3_upload_uri = '/'.join([PROCESSED_DATA_PREFIX, section_name, output_file_name])

            if generic_name not in generic_name_to_file_path.keys():
                generic_name_to_file_path[generic_name] = {}

            if section_name not in generic_name_to_file_path[generic_name].keys():
                generic_name_to_file_path[generic_name][section_name] = []

            generic_name_to_file_path[generic_name][section_name].append(s3_upload_uri)

            upload_file_to_s3(s3_client, BUCKET, s3_upload_uri, content=text)
        break

    large_prescriptions_with_not_seen_generic_name = [
        lp
        for lp in large_prescriptions
        if lp not in generic_name_to_file_path.keys()
    ]

    print(
        f'Prescriptions of size greater than 40KB with not seen generic so far'
        f' {large_prescriptions_with_not_seen_generic_name}')

    ep = {ep for ep in empty_prescriptions if ep not in generic_name_to_file_path.keys()}
    print(f'Number of empty prescriptions not seen so far {len(ep)}')

    print(f'Number of prescriptions without generic name {no_generic_name}')
