import pandas as pd
import os
import argparse
import tqdm


def merge_datasets(arguments):
    folder_name = os.path.basename(arguments.folder_path)
    
    folder_path = arguments.folder_path
    
    file_list = os.listdir(folder_path)
    file_path = os.path.join(folder_path, file_list[0])
    df = pd.read_csv(file_path).drop('Unnamed: 0', axis=1)
    
    for file in tqdm.tqdm(file_list[1:]):
       file_path = os.path.join(folder_path, file)
       temp_df = pd.read_csv(file_path).drop('Unnamed: 0', axis=1)
       df = pd.concat([df, temp_df])
    
    save_path = os.path.join(arguments.output_path, folder_name )
    
    df.to_csv(f'{save_path}_merged.csv')


if __name__ == '__main__':
    args = argparse.ArgumentParser()
    args.add_argument('-f' ,'--folder_path',type=str,help='Path to folder containing smaller dataframes',required=True)
    args.add_argument('-o' ,'--output_path',type=str,default='',help='Path to output folder')
    
    arguments = args.parse_args()
    merge_datasets(arguments)
